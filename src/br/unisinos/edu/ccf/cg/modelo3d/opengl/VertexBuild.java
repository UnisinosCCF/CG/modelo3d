package br.unisinos.edu.ccf.cg.modelo3d.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;

import br.unisinos.edu.ccf.cg.modelo3d.object.Face;
import br.unisinos.edu.ccf.cg.modelo3d.object.Group;
import br.unisinos.edu.ccf.cg.modelo3d.object.Mesh;
import br.unisinos.edu.ccf.cg.modelo3d.util.FormatUtil;

/**
 * Classe responsavel por montar os
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 30 de abr de 2020
 */
public class VertexBuild {

	/**
	 * Método para printar no terminal a estrutura do mesh.
	 * 
	 * @param mesh
	 */
	public static void printMeshStruct(Mesh mesh) {

		System.out.println("Print Mesh ===========================");
		System.out.println("Numero de grupos: " + mesh.getGroups().size());
		for (Vector3f v : mesh.getVertex()) {
			System.out.println("\t V = " + v.toString());
		}
		System.out.println();
		for (Vector2f v : mesh.getMappings()) {
			System.out.println("\t VT = " + v.toString());
		}

		System.out.println();
		for (Vector3f v : mesh.getNormals()) {
			System.out.println("\t VN = " + v.toString());
		}
		for (int i = 0; i < mesh.getGroups().size(); i++) {
			System.out.println("\t\t === Grupo " + i + " " + mesh.getGroups().get(i).getName());
			for (int j = 0; j < mesh.getGroups().get(i).getFaces().size(); j++) {
				System.out.println("\t\t\t === Face " + j);
				for (int k = 0; k < mesh.getGroups().get(i).getFaces().get(j).getNumberVertex(); k++) {
					System.out.println("\t\t\t\t === Vertice " + k);
					System.out.println("\t\t\t\t\t === v: " + mesh.getGroups().get(i).getFaces().get(j).getVerts()[k] + " -> " + mesh.getVertex().get(mesh.getGroups().get(i)
							.getFaces().get(j).getVerts()[k] - 1));
					System.out.println("\t\t\t\t\t === t: " + mesh.getGroups().get(i).getFaces().get(j).getTexts()[k] + " -> " + mesh.getMappings().get(mesh.getGroups().get(i)
							.getFaces().get(j).getTexts()[k] - 1));
					System.out.println("\t\t\t\t\t === n: " + mesh.getGroups().get(i).getFaces().get(j).getNorms()[k] + " -> " + mesh.getNormals().get(mesh.getGroups().get(i)
							.getFaces().get(j).getNorms()[k] - 1));
				}
			}

		}
	}

	/**
	 * Metodo responsavel por montar os vetores e criar o VAO e os VBOs para o
	 * grupo.
	 * 
	 * @param mesh
	 */
	public static void buildVectors(Mesh mesh) {

		// Aqui será montado os vetores que seram passados ao OPENGL
		for (Group group : mesh.getGroups()) {
			List<Float> vs = new ArrayList<>();
			List<Float> vts = new ArrayList<>();
			List<Float> vns = new ArrayList<>();
			for (Face face : group.getFaces()) {
				for (int i = 0; i < (face.getNumberVertex()); i++) {
					if (mesh.getVertex().size() > 0) {
						Vector3f v = mesh.getVertex().get(face.getVerts()[i] - 1);
						vs.add(v.x);
						vs.add(v.y);
						vs.add(v.z);
					}
					if (mesh.getMappings().size() > 0) {
						Vector2f vt = mesh.getMappings().get(face.getTexts()[i] - 1);
						vts.add(vt.x);
						vts.add(vt.y);
					}
					if (mesh.getNormals().size() > 0) {
						Vector3f vn = mesh.getNormals().get(face.getNorms()[i] - 1);
						vns.add(vn.x);
						vns.add(vn.y);
						vns.add(vn.z);
					}
				}
			}

			// Gerando VAO
			group.setVaoID(glGenVertexArrays());
			group.setNumberVertices(vs.size() / 3);

			glBindVertexArray(group.getVaoID());

			// Usando VBOs
			int vertexVBO = glGenBuffers();
			group.setVboVertices(vertexVBO);
			glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
			glBufferData(GL_ARRAY_BUFFER, FormatUtil.listToArray(vs), GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glEnableVertexAttribArray(0);

			int texturesVBO = glGenBuffers();
			group.setVboTextures(texturesVBO);
			glBindBuffer(GL_ARRAY_BUFFER, texturesVBO);
			glBufferData(GL_ARRAY_BUFFER, FormatUtil.listToArray(vts), GL_STATIC_DRAW);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
			glEnableVertexAttribArray(1);

//
//			int normalsVBO = glGenBuffers();
//			group.setVboNormals(normalsVBO);
//			glBindBuffer(GL_ARRAY_BUFFER, normalsVBO);
//			glBufferData(GL_ARRAY_BUFFER, FormatUtil.listToArray(vns), GL_STATIC_DRAW);
//			glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);

			glBindVertexArray(0);

		}

	}
}
