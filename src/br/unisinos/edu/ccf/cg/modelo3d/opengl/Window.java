package br.unisinos.edu.ccf.cg.modelo3d.opengl;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig;
import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig.WindowConfig;

/**
 * Classe responsavel por iniciar a janela e o contexto do opengl.
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 13 de abr de 2020
 */
public class Window {
	private long window;
	private String name;
	private int width;
	private int height;
	private double previous_seconds_FPS = 0;
	private int frame_count_FPS = 0;
	private double control_fps_per_second = 1.0;

	public Window(String name, int height, int width) {
		this.name = name;
		this.height = height;
		this.width = width;
	}

	public Window(String name, WindowConfig config) {
		this.name = name;
		this.height = config.getHeight();
		this.width = config.getWidth();
	}

	/**
	 * Relação de aspecto da janela.
	 * 
	 * @return width/height
	 */
	public float aspectRatio() {
		return (float) width / (float) height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Cria a Janela e inicializa a GLFW
	 */
	public void createWindow() {
		if (!glfwInit()) {
			throw new IllegalStateException("Falha para iniciar GLFW!");
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		window = glfwCreateWindow(width, height, this.name, 0, 0);
		if (window == 0) {
			throw new IllegalStateException("Falha para criar Janela!");
		}
		glfwMakeContextCurrent(window);
		GL.createCapabilities();

		glEnable(GL_TEXTURE_2D);

		GLFWVidMode videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(window, (videoMode.width() - width) / 2, (videoMode.height() - height) / 2);

		glfwShowWindow(window);
	}

	/**
	 * Destrói a janela criada e desativa a glfw
	 */
	public void free() {
		glfwDestroyWindow(window);

		glfwTerminate();
	}

	/**
	 * Atualiza o poll de eventos da janela, retorn false se a janela foi fechada.
	 * 
	 * @return
	 */
	public boolean update() {
		glfwPollEvents();

		return !(glfwWindowShouldClose(window));
	}

	/**
	 * Realiza o swap de buffers na tela.
	 */
	public void swapBuffers() {
		glfwSwapBuffers(window);
	}

	/**
	 * Responsavel por mostrar o FPS no titulo da janela.
	 */
	public void updateFPS() {
		double current_seconds = GLFW.glfwGetTime();
		double elapsed_seconds = current_seconds - previous_seconds_FPS;
		if (elapsed_seconds > control_fps_per_second) {
			double fps = (double) frame_count_FPS / elapsed_seconds;
			GLFW.glfwSetWindowTitle(window, this.getName() + " - @ FPS: " + fps);
			previous_seconds_FPS = current_seconds;
			frame_count_FPS = 0;
		} else {
			frame_count_FPS++;
		}
	}

	public long getID() {
		return this.window;
	}

}