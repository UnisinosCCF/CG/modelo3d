package br.unisinos.edu.ccf.cg.modelo3d.opengl;

import java.util.Date;
import java.util.List;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import br.unisinos.edu.ccf.cg.modelo3d.util.SystemUtil;

/**
 * Classe responsavel pela gestão das transfrmações dos obj3d
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 30 de abr de 2020
 */
public class Transform {
	private Vector3f position;
	private Quaternionf rotation;
	private Vector3f scale;

	//
	private List<Vector3f> animation;
	private int animationPosition;
	private float animationScale;
	private long time = 0;

	public Transform() {
		position = new Vector3f();
		rotation = new Quaternionf();
		scale = new Vector3f(1);
	}

	/**
	 * Retorna a matrix de transformações;
	 * 
	 * @return
	 */
	public Matrix4f getTransformation() {
		Matrix4f returnValue = new Matrix4f();

		returnValue.translate(position);
		returnValue.rotate(rotation);
		returnValue.scale(scale);

		return returnValue;
	}

	public List<Vector3f> getAnimation() {
		return animation;
	}

	public void setAnimation(List<Vector3f> animation, float scale) {
		this.animation = animation;
		this.animationScale = 30;
		this.animationPosition = 0;
	}

	public void nextAnimation() {
		long time2 = new Date().getTime();
		if (SystemUtil.diffTime(time, time2, 50)) {

			if (animationPosition == animation.size() - 1) {
				animationPosition = 0;
			}

			Vector3f current = animation.get(animationPosition++).get(new Vector3f()).mul(animationScale);
			setPosition(current);

			Vector3f next = animation.get(((animationPosition) % animation.size())).get(new Vector3f()).mul(animationScale);

			// rotate
			double angle = SystemUtil.calculoAngulo(current, next);

			setRotation(new Quaternionf());
			getRotation().rotateLocalY((float) angle);

			this.time = time2;
		}
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public Quaternionf getRotation() {
		return rotation;
	}

	public void setRotation(Quaternionf rotation) {
		this.rotation = rotation;
	}

	public Vector3f getScale() {
		return scale;
	}

	public void setScale(Vector3f scale) {
		this.scale = scale;
	}
}