package br.unisinos.edu.ccf.cg.modelo3d.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.joml.Matrix4f;
import org.joml.Vector4f;

/**
 * Classe de carregamento dos shader e montagem do programa.
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 30 de abr de 2020
 */
public class Shader {

	private String shaderName;
	private int vs, fs, program;
	private int projectionLocate;
	private int viewLocate;
	private int modelLocate;
	private int colorLocate;

	public Shader(String shader) {
		this.shaderName = shader;
	}

	/**
	 * Seta uma variavel uniform 1 inteiro.
	 * 
	 * @param name
	 * @param value
	 */
	public void setUniform(String name, int value) {
		int location = glGetUniformLocation(program, name);
		if (location != -1) {
			glUniform1i(location, value);
		}
	}

	/**
	 * Cria o shader.
	 * 
	 * @return
	 */
	public boolean create() {
		int success;

		vs = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vs, readSource(shaderName + ".vs"));
		glCompileShader(vs);
		success = glGetShaderi(vs, GL_COMPILE_STATUS);
		if (success == GL_FALSE) {
			System.err.println(glGetShaderInfoLog(vs));
		}

		fs = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fs, readSource(shaderName + ".fs"));
		glCompileShader(fs);
		success = glGetShaderi(fs, GL_COMPILE_STATUS);
		if (success == GL_FALSE) {
			System.err.println(glGetShaderInfoLog(fs));
		}

		program = glCreateProgram();
		glAttachShader(program, vs);
		glAttachShader(program, fs);

		glLinkProgram(program);
		success = glGetProgrami(program, GL_LINK_STATUS);
		if (success == GL_FALSE) {
			System.err.println(glGetProgramInfoLog(fs));
		}
		glValidateProgram(program);
		success = glGetProgrami(program, GL_VALIDATE_STATUS);
		if (success == GL_FALSE) {
			System.err.println(glGetProgramInfoLog(fs));
		}

		projectionLocate = glGetUniformLocation(program, "projection");
		viewLocate = glGetUniformLocation(program, "view");
		modelLocate = glGetUniformLocation(program, "model");
		colorLocate = glGetUniformLocation(program, "colors");
		return true;
	}

	/**
	 * Destroy o shader
	 */
	public void destroy() {
		glDetachShader(program, vs);
		glDetachShader(program, fs);
		glDeleteShader(vs);
		glDeleteShader(fs);
		glDeleteProgram(program);
	}

	/**
	 * usa o Shader.
	 */
	public void useShader() {

		glUseProgram(program);
	}

	/**
	 * Seta a matrix de visão e projeção a partir da camera.
	 * 
	 * @param matrix4f
	 */
	public void setCamera(Camera camera, Window window) {
		if (viewLocate != -1) {
			Matrix4f projection = new Matrix4f().setPerspective((float) Math.toRadians(camera.getZoom()), window.aspectRatio(), 0.1f, 500.0f);
			setProjection(projection);
			setView(camera.getView());
		}
	}

	/**
	 * Seta a matrix de projeção. se usar o set camera não é necessario utilizar
	 * este.
	 * 
	 * @param matrix4f
	 */
	public void setProjection(Matrix4f matrix4f) {
		if (projectionLocate != -1) {
			float matrix[] = new float[16];
			matrix4f.get(matrix);
			glUniformMatrix4fv(projectionLocate, false, matrix);
		}
	}

	/**
	 * Seta a matrix de visão. se usar o set camera não é necessario utilizar este.
	 * 
	 * @param matrix4f
	 */
	public void setView(Matrix4f matrix4f) {
		if (viewLocate != -1) {
			float matrix[] = new float[16];
			matrix4f.get(matrix);
			glUniformMatrix4fv(viewLocate, false, matrix);
		}
	}

	/**
	 * Seta o vetor de cores.
	 * 
	 * @param vec4
	 */
	public void setColor(Vector4f vec4) {
		if (colorLocate != -1) {
			glUniform4f(colorLocate, vec4.x, vec4.y, vec4.z, vec4.w);
		}
	}

	/**
	 * Seta a matrix de transformação.
	 * 
	 * @param transform
	 */
	public void setModel(Transform transform) {
		if (modelLocate != -1) {
			float matrix[] = new float[16];
			transform.getTransformation().get(matrix);
			glUniformMatrix4fv(modelLocate, false, matrix);
		}
	}

//	public void setTransform(Transform transform) {
//		if (uniMatTransformObject != -1) {
//			float matrix[] = new float[16];
//			transform.getTransformation().get(matrix);
//			glUniformMatrix4fv(uniMatTransformObject, false, matrix);
//		}
//	}

	private String readSource(String file) {
		BufferedReader reader = null;
		StringBuilder sourceBuilder = new StringBuilder();

		try {
			// System.out.println(getClass().getResourceAsStream("/shaders/" + file));
			reader = new BufferedReader(new FileReader(new File("./shaders/" + file)));

			String line;

			while ((line = reader.readLine()) != null) {
				sourceBuilder.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sourceBuilder.toString();

	}
}
