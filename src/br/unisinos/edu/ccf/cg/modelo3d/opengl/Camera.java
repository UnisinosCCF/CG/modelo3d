package br.unisinos.edu.ccf.cg.modelo3d.opengl;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Camera {

	public final float YAW = -90.0f;
	public final float PITCH = 0.0f;
	public final float SPEED = 3.5f;
	public final float ZOOM = 45.0f;

	private Vector3f position;
	private Vector3f front;
	private Vector3f up;
	private Vector3f right;
	private Vector3f worldUp;

	private float yaw;
	private float pitch;
	// Camera options
	private float movementSpeed;
	private float zoom;

	public Camera() {
		this.position = new Vector3f(0.0f, 0.0f, 0.0f);
		this.yaw = YAW;
		this.pitch = PITCH;
		this.front = (new Vector3f(0.0f, 0.0f, -1.0f));
		this.movementSpeed = (SPEED);
		this.zoom = (ZOOM);
		this.right = new Vector3f();
		this.worldUp = new Vector3f(0.0f, 1.0f, 0.0f);
		updateCameraVectors();
	}

	public Camera(Vector3f eyePosition, Vector3f dirFront, Vector3f up) {
		this.position = eyePosition;
		this.yaw = YAW;
		this.pitch = PITCH;
		this.front = dirFront;
		this.movementSpeed = (SPEED);
		this.zoom = (ZOOM);
		this.right = new Vector3f();
		this.worldUp = up;
		updateCameraVectors();
	}

	public Camera(Vector3f position, Vector3f up, float yaw, float pitch) {
		this.position = position;
		this.yaw = yaw;
		this.pitch = pitch;
		this.front = (new Vector3f(0.0f, 0.0f, -1.0f));
		this.movementSpeed = (SPEED);
		this.zoom = (ZOOM);
		this.right = new Vector3f();
		this.worldUp = up;
		updateCameraVectors();
	}

	/**
	 * Monta a matrix de lookAt (view)
	 * 
	 * @return
	 */
	public Matrix4f getView() {
		return new Matrix4f().lookAt(this.position, this.position.add(this.front, new Vector3f()), this.up);
	}

	/**
	 * Realiza processamento do teclado.
	 * 
	 * @param direction
	 * @param deltaTime
	 */
	public void processKeyboard(int direction, float deltaTime) {
		float velocity = this.movementSpeed * deltaTime;
		if (direction == 0) {
			this.position.add(this.front.mul(velocity, new Vector3f()));
		} else if (direction == 1) {
			this.position.sub(this.front.mul(velocity, new Vector3f()));
		} else if (direction == 2) {
			this.position.sub(this.right.mul(velocity, new Vector3f()));
		} else if (direction == 3) {
			this.position.add(this.right.mul(velocity, new Vector3f()));
		} else if (direction == 4) {
			this.yaw -= 0.25f;
			updateCameraVectors();
		} else if (direction == 5) {
			this.yaw += 0.25f;
			updateCameraVectors();
		}
	}

	private void updateCameraVectors() {
		// Calculate the new Front vector
		Vector3f front = new Vector3f();
		front.x = (float) ((Math.cos((float) Math.toRadians(this.yaw))) * (Math.cos((float) Math.toRadians(this.pitch))));
		front.y = (float) (Math.sin((float) Math.toRadians(this.pitch)));
		front.z = (float) ((Math.sin((float) Math.toRadians(this.yaw))) * (Math.cos((float) Math.toRadians(this.pitch))));
		this.front = front.normalize();
		// Also re-calculate the Right and Up vector
		this.right = this.front.cross(this.worldUp, new Vector3f()).normalize();
		this.up = this.right.cross(this.front, new Vector3f()).normalize();
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public Vector3f getFront() {
		return front;
	}

	public void setFront(Vector3f front) {
		this.front = front;
	}

	public Vector3f getUp() {
		return up;
	}

	public void setUp(Vector3f up) {
		this.up = up;
	}

	public Vector3f getRight() {
		return right;
	}

	public void setRight(Vector3f right) {
		this.right = right;
	}

	public Vector3f getWorldUp() {
		return worldUp;
	}

	public void setWorldUp(Vector3f worldUp) {
		this.worldUp = worldUp;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getMovementSpeed() {
		return movementSpeed;
	}

	public void setMovementSpeed(float movementSpeed) {
		this.movementSpeed = movementSpeed;
	}

	public float getZoom() {
		return zoom;
	}

	public void setZoom(float zoom) {
		this.zoom = zoom;
	}

}