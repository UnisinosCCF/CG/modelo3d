package br.unisinos.edu.ccf.cg.modelo3d.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;

/**
 * Classe responsavel pelas Texturas, cada instancia representa uma textura.
 * 
 * @author Cristiano Farias <cristiano.farias@multi24h.com.br>
 * @since 28 de mar de 2020
 */
public class Texture {

	private int id;
	private int width;
	private int heigth;

	public int getId() {
		return id;
	}

	public Texture(String filename) {
		BufferedImage bi;
		try {
			bi = ImageIO.read(new File(filename));
			width = bi.getWidth();
			heigth = bi.getHeight();

			int[] pixels_raw = new int[width * heigth * 4];

			pixels_raw = bi.getRGB(0, 0, width, heigth, null, 0, width);

			ByteBuffer pixels = BufferUtils.createByteBuffer(width * heigth * 4);

			for (int i = 0; i < width; i++) {
				for (int j = 0; j < heigth; j++) {
					int pixel = pixels_raw[i * width + j];
					pixels.put((byte) ((pixel >> 16) & 0xFF)); // RED
					pixels.put((byte) ((pixel >> 8) & 0xFF)); // GREEN
					pixels.put((byte) (pixel & 0xFF)); // BLUE
					pixels.put((byte) ((pixel >> 24) & 0xFF)); // ALPHA

				}
			}
			pixels.flip();

			id = glGenTextures();
			bind(0);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			float borderColor[] = { 1.0f, 1.0f, 0.0f, 1.0f };
			glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, heigth, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// Indica que a textura deve ser colocada para desenho.
	public void bind() {
		bind(0);
	}

	// Indica que a textura deve ser colocada para desenho.
	public void bind(int sampler) {
		if (sampler >= 0 && sampler <= 31) {
			glActiveTexture(GL_TEXTURE0 + sampler);
			glBindTexture(GL_TEXTURE_2D, id);
		}
	}
}
