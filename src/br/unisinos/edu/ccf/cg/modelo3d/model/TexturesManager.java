package br.unisinos.edu.ccf.cg.modelo3d.model;

import java.util.ArrayList;
import java.util.List;

import br.unisinos.edu.ccf.cg.modelo3d.opengl.Texture;

/**
 * Classe de gerencia de texturas. Esta classe utiliza o padrão singleton.
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 14 de jun de 2020
 */
public class TexturesManager {

	private static TexturesManager instance;

	private List<Texture> textures;

	public TexturesManager() {
		this.textures = new ArrayList<Texture>();
	}

	public static synchronized TexturesManager getInstance() {
		if (instance == null) {
			instance = new TexturesManager();
		}
		return instance;
	}

	public Integer newTexture(String name) {
		Texture texture = new Texture("./textures/" + name);
		this.textures.add(texture);
		return texture.getId();
	}

	public Texture getTexture(int idTexture) {
		for (Texture texture : textures) {
			if (texture.getId() == idTexture) {
				return texture;
			}
		}
		return null;
	}

}
