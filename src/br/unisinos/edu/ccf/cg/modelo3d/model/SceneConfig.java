package br.unisinos.edu.ccf.cg.modelo3d.model;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;

/**
 * Classe que representa o obj de configuração da cena
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 1 de mai de 2020
 */
public class SceneConfig {

	private List<MeshConfig> meshes;
	private List<Object3DConfig> objs;
	private CameraConfig camera;
	private WindowConfig window;

	public SceneConfig() {
		super();
		this.meshes = new ArrayList<SceneConfig.MeshConfig>();
		this.objs = new ArrayList<SceneConfig.Object3DConfig>();
	}

	public List<MeshConfig> getMeshes() {
		return meshes;
	}

	public void setMeshes(List<MeshConfig> meshes) {
		this.meshes = meshes;
	}

	public List<Object3DConfig> getObjs() {
		return objs;
	}

	public void setObjs(List<Object3DConfig> objs) {
		this.objs = objs;
	}

	public CameraConfig getCamera() {
		return camera;
	}

	public void setCamera(CameraConfig camera) {
		this.camera = camera;
	}

	public WindowConfig getWindow() {
		return window;
	}

	public void setWindow(WindowConfig window) {
		this.window = window;
	}

	public class Object3DConfig {

		private String name;
		private String meshName;
		private Vector3f translationStart;
		private float scaleStart;
		private float rotationStart;
		private int variable1;
		private String variable2;

		public String getVariable2() {
			return variable2;
		}

		public void setVariable2(String variable2) {
			this.variable2 = variable2;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getMeshName() {
			return meshName;
		}

		public void setMeshName(String meshName) {
			this.meshName = meshName;
		}

		public Vector3f getTranslationStart() {
			return translationStart;
		}

		public void setTranslationStart(Vector3f translationStart) {
			this.translationStart = translationStart;
		}

		public float getScaleStart() {
			return scaleStart;
		}

		public void setScaleStart(float scaleStart) {
			this.scaleStart = scaleStart;
		}

		public float getRotationStart() {
			return rotationStart;
		}

		public void setRotationStart(float rotationStart) {
			this.rotationStart = rotationStart;
		}

		public int getVariable() {
			return variable1;
		}

		public void setVariable(int variable) {
			this.variable1 = variable;
		}

	}

	public class CameraConfig {
		private Vector3f eye;
		private Vector3f dir;
		private Vector3f up;

		public Vector3f getEye() {
			return eye;
		}

		public void setEye(Vector3f eye) {
			this.eye = eye;
		}

		public Vector3f getDir() {
			return dir;
		}

		public void setDir(Vector3f dir) {
			this.dir = dir;
		}

		public Vector3f getUp() {
			return up;
		}

		public void setUp(Vector3f up) {
			this.up = up;
		}

	}

	public class WindowConfig {
		private Integer height;
		private Integer width;

		public Integer getHeight() {
			return height;
		}

		public void setHeight(Integer height) {
			this.height = height;
		}

		public Integer getWidth() {
			return width;
		}

		public void setWidth(Integer width) {
			this.width = width;
		}

	}

	public class MeshConfig {
		private String name;
		private String path;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

	}
}
