package br.unisinos.edu.ccf.cg.modelo3d.model;

import java.util.List;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.joml.Vector4f;

import br.unisinos.edu.ccf.cg.modelo3d.object.Mesh;
import br.unisinos.edu.ccf.cg.modelo3d.object.Obj3D;
import br.unisinos.edu.ccf.cg.modelo3d.opengl.Camera;

/**
 * Classe que armazena o obj tiro e suas ações.
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 1 de mai de 2020
 */
public class ShotModel {

	public final static String MESH_NAME = "Shot";
	private Obj3D object;
	private Vector3f position;
	private Vector3f front;
	private Vector3f up;

	private Vector3f shotFront;
	private float shotLimit = 300;
	private float shotSped = 0.1f;

	public ShotModel start(List<Mesh> meshes) {
		object = ReadValues.mountObject3D("Bala1", MESH_NAME, meshes);
		object.getTransform().getPosition().get(new Vector3f());
		object.getTransform().getScale().get(new Vector3f());
		object.getTransform().getRotation().rotateLocalX((float) Math.toRadians(-90));
		object.setVariavel(1);
		object.setDrawThis(false);
		object.setColors(new Vector4f(0.500f, 0.705f, 0.650f, 1));
		return this;
	}

	/**
	 * Direção da camera.
	 * 
	 * @param camera
	 */
	public void setFromCamera(Camera camera) {
		this.position = camera.getPosition();
		this.front = camera.getFront();
		this.up = camera.getFront();
	}

	public float shotDistance() {
		return new Vector3f().distance(object.getTransform().getPosition());
	}

	private boolean collisionAction(Obj3D obj, Vector3f collisionPoint) {
		if (obj.getVariavel() <= -2) {
			// Não acontece nada
			return false;
		} else if (obj.getVariavel() == -1) {
			reflexion(obj, collisionPoint);
			System.out.println("\nREFELXÃO COM OBJ: " + obj.getName());
			return true;
		} else if (obj.getVariavel() == 0) {
			obj.setDrawThis(false);
			object.setDrawThis(false);
			System.out.println("\nDESTRUIDO OBJ: " + obj.getName());
			return true;
		} else {
			obj.setVariavel(obj.getVariavel() - 1);
			obj.getColors().add(0.2f, -0.2f, -0.2f, 0);
			object.setDrawThis(false);
			System.out.println("\nCOLISÃO COM OBJ: " + obj.getName() + " [vida restante: " + (obj.getVariavel() - 1) + "]");
			return true;
		}
	}

//	public void checkCollision(List<Obj3D> objects) {
//		for (Obj3D obj3d : objects) {
//			if (!obj3d.equals(object) && obj3d.isDrawThis()) {
//				Vector3f VP = obj3d.getPM().sub(object.getC(), new Vector3f());
//				Vector3f P = VP.absolute().mul(object.getR()).add(object.getC());
//				boolean test = P.x > obj3d.getMin().x && P.y > obj3d.getMin().y && P.z > obj3d.getMin().z;
//				test = test && P.x < obj3d.getMax().x && P.y < obj3d.getMax().y && P.z < obj3d.getMax().z;
//				if (test) {
//					System.out.println("\nCOLISÃO COM OBJ: " + obj3d.getName());
//					collisionAction(obj3d, P);
//					break;
//				}
//			}
//		}
//	}

	public void checkCollision(List<Obj3D> objects) {
		for (Obj3D obj3d : objects) {
			if (!obj3d.equals(object) && obj3d.isDrawThis()) {
				float d = object.getC().distance(obj3d.getC());
				if (d < (object.getR() + obj3d.getR())) {
					Vector3f VP = obj3d.getPM().sub(object.getC(), new Vector3f());
					Vector3f P = VP.absolute().mul(object.getR()).add(object.getC());
					if (collisionAction(obj3d, P)) {
						break;
					}
				}
			}
		}
	}

	/**
	 * 
	 * realiza rotaçaõ em X no angulo informado.
	 * 
	 * @param angle
	 */
	public void rotationX(int angle) {
		object.getTransform().getRotation().rotateLocalX((float) Math.toRadians(angle));
	}

	/**
	 * Realiza soma na velocidade do projetil.
	 * 
	 * @param speed
	 */
	public void sumSpeed(float speed) {
		if (this.shotSped < 0.005f) {
			this.shotSped = 0.005f;
		} else {
			this.shotSped = this.shotSped + (speed);
		}
		System.out.println("SHOT SPEAD -> " + this.shotSped);
	}

	public void fire(Vector3f position, Vector3f front, float rotation) {
		if (!object.isDrawThis()) {
			object.setDrawThis(true);
			object.getTransform().setPosition(position);
			// default para virar a bala.
			object.getTransform().setRotation(new Quaternionf().rotateLocalX(((float) Math.toRadians(-90))));
			object.getTransform().getRotation().rotateLocalY(-(float) Math.toRadians(rotation));
			shotFront = (front);
			// shotCount = 0f;
			System.out.println("TIRO!!!");
		}
	}

	public void next(List<Obj3D> objects) {
		if (object.isDrawThis()) {
			if (shotDistance() > shotLimit) {
				object.setDrawThis(false);
				System.out.println("\nBALA PERDIDA......");
			} else {
				// shot.getTransform().getRotation().rotateLocalZ(0.100f);
				object.getTransform().getPosition().add(this.shotFront.mul(this.shotSped, new Vector3f()));
				System.out.print(">");
				// System.out.println("SHOT DISTANCE: " + shotDistance());
				checkCollision(objects);
			}
		}
	}

	public void reflexion(Obj3D obj, Vector3f collisionPoint) {
		// Calcula a normal
		Vector3f normal = (collisionPoint.sub(obj.getC(), new Vector3f()).div(obj.getR()));
		// Reflete
		this.shotFront.reflect(normal);
		System.out.println("Refletiuuu!");
	}

	/**
	 * Busca a matrix de visao para a bala.
	 * 
	 * @return
	 */
	public Matrix4f getView() {
		return new Matrix4f().lookAt(this.position, this.position.add(this.front, new Vector3f()), this.up);
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public Vector3f getFront() {
		return front;
	}

	public void setFront(Vector3f front) {
		this.front = front;
	}

	public Vector3f getUp() {
		return up;
	}

	public void setUp(Vector3f up) {
		this.up = up;
	}

	public Obj3D getObject() {
		return object;
	}

	public void setObject(Obj3D object) {
		this.object = object;
	}

}
