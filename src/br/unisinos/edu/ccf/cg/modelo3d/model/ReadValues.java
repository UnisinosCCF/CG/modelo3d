package br.unisinos.edu.ccf.cg.modelo3d.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.joml.Vector3f;

import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig.CameraConfig;
import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig.MeshConfig;
import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig.Object3DConfig;
import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig.WindowConfig;
import br.unisinos.edu.ccf.cg.modelo3d.object.Face;
import br.unisinos.edu.ccf.cg.modelo3d.object.Group;
import br.unisinos.edu.ccf.cg.modelo3d.object.Material;
import br.unisinos.edu.ccf.cg.modelo3d.object.Mesh;
import br.unisinos.edu.ccf.cg.modelo3d.object.Obj3D;
import br.unisinos.edu.ccf.cg.modelo3d.util.FormatUtil;
import br.unisinos.edu.ccf.cg.modelo3d.util.ReadUtil;
import br.unisinos.edu.ccf.cg.modelo3d.util.SystemUtil;

/**
 * Classe de leitura dos valores dos arquivos e montagens..
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 1 de mai de 2020
 */
public class ReadValues {

	/**
	 * Metodo se montagem de objeto a partir de uma meshName
	 * 
	 * @param meshName
	 * @param meshList
	 * @return
	 */
	public static Obj3D mountObject3D(String objName, String meshName, List<Mesh> meshList) {
		for (int i = 0; i < meshList.size(); i++) {
			if (meshList.get(i).getName().equals(meshName)) {
				Obj3D obj3d = new Obj3D();
				obj3d.setMesh(meshList.get(i));
				obj3d.setName(objName);
				return obj3d;
			}
		}
		SystemUtil.endProgram("Mesh não encontrado para montagem do Objeto 3D");
		return null;

	}

	/**
	 * Método de leitura do arquivo de Material.
	 * 
	 * @param configFile
	 * @return
	 */
	public static List<Material> readMaterial(String materialFile) {
		try {
			List<Material> materials = new ArrayList<Material>();

			File file = new File(materialFile);

			if (!file.exists() || file.isDirectory()) {
				SystemUtil.endProgram("Arquivo informado não é mtl valido ou não foi encontrado: " + file.getAbsolutePath());
			}

			List<String> lines = ReadUtil.readFileToList(file, false, "UTF-8");
			Material material = null;

			// loop
			for (int i = 0; i < lines.size(); i++) {
				String line = lines.get(i).trim();
				if (line.startsWith("newmtl ")) {
					// É inicio - Adicionar a lista
					line = FormatUtil.startCut(line, "newmtl");
					material = new Material(line);
					materials.add(material);
				} else if (line.startsWith("Ka ")) {
					line = FormatUtil.startCut(line, "Ka");
					String split[] = line.split(" ");
					if (split.length < 3) {
						SystemUtil.endProgram("É necessario ao menos 3");
					}
					float ka[] = FormatUtil.toFloatArray(split[0], split[1], split[2]);
					material.setKa(new Vector3f(ka));
				} else if (line.startsWith("Kd ")) {
					line = FormatUtil.startCut(line, "Kd");
					String split[] = line.split(" ");
					if (split.length < 3) {
						SystemUtil.endProgram("É necessario ao menos 3");
					}
					float kd[] = FormatUtil.toFloatArray(split[0], split[1], split[2]);
					material.setKd(new Vector3f(kd));
				} else if (line.startsWith("Ks ")) {
					line = FormatUtil.startCut(line, "Ks");
					String split[] = line.split(" ");
					if (split.length < 3) {
						SystemUtil.endProgram("É necessario ao menos 3");
					}
					float ks[] = FormatUtil.toFloatArray(split[0], split[1], split[2]);
					material.setKs(new Vector3f(ks));
				} else if (line.startsWith("Ns ")) {
					line = FormatUtil.startCut(line, "Ns");
					String split[] = line.split(" ");
					if (split.length != 1) {
						SystemUtil.endProgram("É necessario 1 argumento");
					}
					float ns = FormatUtil.toFloat(split[0]);
					material.setNs(ns);
				} else if (line.startsWith("map_Kd ")) {
					line = FormatUtil.startCut(line, "map_Kd ");
					material.setMap(line);
				}
			}
			return materials;
		} catch (IOException | NumberFormatException |

				ClassCastException e) {
			JOptionPane.showMessageDialog(null, "Falha ao realizar leitura de MTL", "Atenção", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
			return null;
		}

	}

	public static List<Vector3f> readAnimacao(String animacaoFile) {
		try {
			List<Vector3f> vecs = new ArrayList<Vector3f>();

			File file = new File(animacaoFile);

			if (!file.exists() || file.isDirectory()) {
				SystemUtil.endProgram("Arquivo informado não é um animação valido ou não foi encontrado: " + file.getAbsolutePath());
			}

			List<String> lines = ReadUtil.readFileToList(file, false, "UTF-8");

			// loop
			for (int i = 0; i < lines.size(); i++) {
				String line = lines.get(i).trim();
				if (line.startsWith("v ")) {
					// É inicio - Adicionar a lista
					line = FormatUtil.startCut(line, "v");
					String split[] = line.split(" ");
					if (split.length < 3) {
						SystemUtil.endProgram("É necessario ao menos 3");
					}
					float v[] = FormatUtil.toFloatArray(split[0], split[1], split[2]);
					vecs.add(new Vector3f(v));
				}
			}
			return vecs;
		} catch (IOException | NumberFormatException | ClassCastException e) {
			JOptionPane.showMessageDialog(null, "Falha ao realizar leitura de Arquivo", "Atenção", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
			return null;
		}

	}

	/**
	 * Método de leitura do arquivo de configuração.
	 * 
	 * @param configFile
	 * @return
	 */
	public static SceneConfig readConfig(String configFile) {
		try {
			File file = new File(configFile);

			if (!file.exists() || file.isDirectory()) {
				SystemUtil.endProgram("Arquivo informado não é config valido ou não foi encontrado: " + file.getAbsolutePath());
			}

			List<String> lines = ReadUtil.readFileToList(file, false, "UTF-8");
			SceneConfig config = new SceneConfig();

			// loop
			for (int i = 0; i < lines.size(); i++) {
				String line = lines.get(i).trim();
				if (line.startsWith("M ")) {
					// É Malha - Adicionar a lista
					line = FormatUtil.startCut(line, "M");
					String split[] = line.split(" ");
					if (split.length != 2) {
						SystemUtil.endProgram("É necessário 2 atributos.");
					}
					MeshConfig meshConfig = new SceneConfig().new MeshConfig();
					meshConfig.setName(split[0]);
					meshConfig.setPath(split[1]);
					config.getMeshes().add(meshConfig);
				} else if (line.startsWith("O ")) {
					// É Malha - Adicionar a lista
					line = FormatUtil.startCut(line, "O");
					String split[] = line.split(" ");
					if (split.length < 8 || split.length > 9) {
						SystemUtil.endProgram("É necessario 8 ou 9 atributos.");
					}
					Object3DConfig object3dConfig = new SceneConfig().new Object3DConfig();
					object3dConfig.setName(split[0]);
					object3dConfig.setMeshName(split[1]);
					float t[] = FormatUtil.toFloatArray(split[2], split[3], split[4]);
					object3dConfig.setTranslationStart(new Vector3f(t));
					object3dConfig.setScaleStart(Float.parseFloat((split[5])));
					object3dConfig.setRotationStart(Float.parseFloat((split[6])));
					object3dConfig.setVariable(Integer.parseInt((split[7])));
					if (split.length == 9) {
						object3dConfig.setVariable2(((split[8])));
					}
					config.getObjs().add(object3dConfig);
				} else if (line.startsWith("C ")) {
					// É Malha - Adicionar a lista
					line = FormatUtil.startCut(line, "C");
					String split[] = line.split(" ");
					if (split.length != 9) {
						SystemUtil.endProgram("É necessario 9 atributos.");
					}
					CameraConfig cameraConfig = new SceneConfig().new CameraConfig();
					float eye[] = FormatUtil.toFloatArray(split[0], split[1], split[2]);
					cameraConfig.setEye(new Vector3f(eye));
					float dir[] = FormatUtil.toFloatArray(split[3], split[4], split[5]);
					cameraConfig.setDir(new Vector3f(dir));
					float up[] = FormatUtil.toFloatArray(split[6], split[7], split[8]);
					cameraConfig.setUp(new Vector3f(up));
					config.setCamera(cameraConfig);
				} else if (line.startsWith("W ")) {
					// É Malha - Adicionar a lista
					line = FormatUtil.startCut(line, "W");
					String split[] = line.split(" ");
					if (split.length != 2) {
						SystemUtil.endProgram("É necessario 2 atributos.");
					}
					WindowConfig windowConfig = new SceneConfig().new WindowConfig();
					windowConfig.setHeight(Integer.parseInt(split[0]));
					windowConfig.setWidth(Integer.parseInt(split[1]));
					config.setWindow(windowConfig);
				}

			}
			return config;

		} catch (IOException | NumberFormatException | ClassCastException e) {
			JOptionPane.showMessageDialog(null, "Falha ao realizar leitura de config", "Atenção", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
			return null;
		}
	}

	/**
	 * Método de leitura da Mesh.
	 * 
	 * @param name
	 * @param fileMesh
	 * @return
	 */
	public static Mesh readMesh(String name, String fileMesh, Map<String, String> materialNames) {
		boolean alertVerticesMessage = false;
		try {

			File file = new File(fileMesh);

			if (!file.exists() || file.isDirectory()) {
				SystemUtil.endProgram("Arquivo informado não é um mesh valido ou não foi encontrado: " + file.getAbsolutePath());
			}

			Mesh mesh = new Mesh(name);

			List<String> lines = ReadUtil.readFileToList(fileMesh, false, "UTF-8");

			// Se cria o grupo default para iniciar o fluxo;
			Group group = null;

			// loop
			for (int i = 0; i < lines.size(); i++) {
				String line = lines.get(i).trim();
				// Indica o nome dos arquivos de Material que seram carregados.
				if (line.startsWith("mtllib ")) {
					line = FormatUtil.startCut(line, "mtllib");
					materialNames.put(line, line);
				} else if (line.startsWith("v ")) {
					// É vertice - Adicionar a malha
					line = FormatUtil.startCut(line, "v");
					String split[] = line.split(" ");
					mesh.addVertex(FormatUtil.toFloatArray(split));
				} else if (line.startsWith("vt ")) {
					// É textura - Adicionar a malha
					line = FormatUtil.startCut(line, "vt");
					String split[] = line.split(" ");
					mesh.addMapping(FormatUtil.toFloatArray(split));
				} else if (line.startsWith("vn ")) {
					// É normal - Adicionar a malha
					line = FormatUtil.startCut(line, "vn");
					String split[] = line.split(" ");
					mesh.addNormal(FormatUtil.toFloatArray(split));
				} else if (line.startsWith("g ")) {
					// É grupo - Adicionar o grupo atual na malha
					line = FormatUtil.startCut(line, "g");
					group = mesh.getGroupUnity(line);
				} else if (line.startsWith("usemtl ")) {
					// É material utilziada no grupo - Adicionar ao grupo atual
					line = FormatUtil.startCut(line, "usemtl");

					// Verifica grupo default
					if (group == null) {
						group = new Group("default");
					}
					group.setMaterial(line);
				} else if (line.startsWith("f ")) {
					// É face - Adicionar indices em nova face e add no grupo atual;
					line = FormatUtil.startCut(line, "f");
					String splitFaceLine[] = line.split(" ");
					boolean isIvVertices = false;
					if (splitFaceLine.length < 3) {
						SystemUtil.endProgram("É necessario ao menos 3");
					} else if (splitFaceLine.length >= 4) {
						if (splitFaceLine.length > 4) {
							alertVerticesMessage = true;
						}
						isIvVertices = true;
					}

					// Cria Face com base na quantidade de vertices
					Face face = new Face(splitFaceLine.length);

					for (int j = 0; j < splitFaceLine.length; j++) {
						// Split v t n
						String splitFace[] = splitFaceLine[j].split("/");
						// Identifica o tipo
						if (splitFace.length == 3) {
							// Pode ser v1/t1/n3 ou v1//n3
							face.getVerts()[j] = Integer.parseInt(splitFace[0]);
							// vericar do meio
							if (!(splitFace[1] == null || splitFace[1].equalsIgnoreCase(""))) {
								face.getTexts()[j] = Integer.parseInt(splitFace[1]);
							}
							// normal
							face.getNorms()[j] = Integer.parseInt(splitFace[2]);
						} else if (splitFace.length == 2) {
							// é v1/t1
							face.getVerts()[j] = Integer.parseInt(splitFace[0]);
							face.getTexts()[j] = Integer.parseInt(splitFace[1]);
						} else {
							// Padrão invalido
							SystemUtil.endProgram("Padrão inválido");
						}
					}
					// Verifica grupo default
					if (group == null) {
						group = new Group("default");
					}

					// Verifica se a face é de 4 vertices
					if (isIvVertices) {
						// Separa em duas faces
						Face f1 = fuorFaceToThree(face, 0, 1, 2);
						Face f2 = fuorFaceToThree(face, 2, 3, 0);
						group.addFace(f1);
						group.addFace(f2);
					} else {
						// Se for de 3 vertices adiciona direto.
						group.addFace(face);
					}
				}

			}
			if (alertVerticesMessage) {
				SystemUtil.message("Foi identificados mais vertices por face que o permitido no mesh: " + name + "\nSerá considerado apenas os 4 primeiros.");
			}
			return mesh;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Falha ao realizar leitura de MESH", "Atenção", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
			return null;
		}
	}

	/**
	 * Método de auxilio para transformar uma face de 4 vertices para duas faces de
	 * 3 vertices cada.
	 * 
	 * @param fuorFace
	 * @param indice1
	 * @param indice2
	 * @param indice3
	 * @return
	 */
	private static Face fuorFaceToThree(Face fuorFace, int indice1, int indice2, int indice3) {
		Face fThree = new Face(3);
		// Verts
		fThree.getVerts()[0] = fuorFace.getVerts()[indice1];
		fThree.getVerts()[1] = fuorFace.getVerts()[indice2];
		fThree.getVerts()[2] = fuorFace.getVerts()[indice3];
		// texts
		fThree.getTexts()[0] = fuorFace.getTexts()[indice1];
		fThree.getTexts()[1] = fuorFace.getTexts()[indice2];
		fThree.getTexts()[2] = fuorFace.getTexts()[indice3];
		// Norml
		fThree.getNorms()[0] = fuorFace.getNorms()[indice1];
		fThree.getNorms()[1] = fuorFace.getNorms()[indice2];
		fThree.getNorms()[2] = fuorFace.getNorms()[indice3];

		return fThree;
	}

}
