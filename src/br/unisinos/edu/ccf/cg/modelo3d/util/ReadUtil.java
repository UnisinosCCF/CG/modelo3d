package br.unisinos.edu.ccf.cg.modelo3d.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Classe auziliar para realizar leituras de 
 */
public class ReadUtil {
	/**
	 * Metodo que realiza a leitura do arquivo de texto e retorna uma string
	 * conteudo o conteudo atributos sem quebra de linha;
	 *
	 * @return String em padrao csv para carregamento do objeto Settings.
	 * @throws IOException
	 */
	public static List<String> readFileToList(String dirFile, Boolean readWhiteLine, String charset) throws IOException {
		return readFileToList(new File(dirFile), readWhiteLine, charset);
	}

	/**
	 * Metodo que realiza a leitura do arquivo de texto e retorna uma string
	 * conteudo o conteudo atributos sem quebra de linha;
	 *
	 * @return String em padrao csv para carregamento do objeto Settings.
	 * @throws IOException
	 */
	public static List<String> readFileToList(File file, Boolean readWhiteLine, String charset) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		InputStreamReader isr = new InputStreamReader(fis, charset);
		BufferedReader br = new BufferedReader(isr);
		String line = "";
		List<String> lines = new ArrayList<String>();
		while ((line = br.readLine()) != null) {
			if (line.trim().equalsIgnoreCase("")) {
				if (readWhiteLine) {
					lines.add(line);
				}
			} else {
				lines.add(line);
			}
		}
		isr.close();
		br.close();
		fis.close();
		return lines;
	}
}
