package br.unisinos.edu.ccf.cg.modelo3d.util;

import java.util.List;

import org.joml.Vector3f;
import org.joml.Vector4f;

/**
 * Classe auxiliar para realizar formtações em tipos;
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 30 de abr de 2020
 */
public class FormatUtil {

	/**
	 * Corta o inicio e o fim da palavra (caso existam)S
	 * 
	 * @param value    palavra
	 * @param startCut inicio a ser cortado
	 * @param endCut   fim a ser cortado.
	 * @return palavra cortada.
	 */
	public static String cut(String value, String startCut, String endCut) {
		value = startCut(value, startCut);
		value = endCut(value, endCut);
		return value;
	}

	/**
	 * Corta o inicio da palavra de acordo com o informado..
	 * 
	 * @param value    palavra.
	 * @param startCut corte inicial (caso exista).
	 * @return
	 */
	public static String startCut(String value, String startCut) {
		if (value.startsWith(startCut)) {
			value = value.substring(startCut.length());
		}
		return value.trim();
	}

	/**
	 * Corta o final da palavra de acordo com o infromado
	 * 
	 * @param value  palavra.
	 * @param endCut Corte final (caso exista)
	 * @return
	 */
	public static String endCut(String value, String endCut) {
		if (value.endsWith(endCut)) {
			value = value.substring(0, value.indexOf(endCut));
		}
		return value.trim();
	}

	/**
	 * Converte um array de strings em um aray de floats
	 * 
	 * @param values
	 * @return
	 */
	public static float[] toFloatArray(String... values) {
		if (values != null) {
			float[] floats = new float[values.length];
			for (int i = 0; i < values.length; i++) {
				floats[i] = toFloat(values[i]);
			}
			return floats;
		}
		return null;
	}

	/**
	 * Converte um String em Float.
	 * 
	 * @param value
	 * @return
	 */
	public static float toFloat(String value) {
		float flo = 0;
		try {
			flo = Float.parseFloat(value);
		} catch (NullPointerException | NumberFormatException e) {
			SystemUtil.endProgram("Não é possivel transformar o texto '" + value + "' em float");
		}
		return flo;
	}

	/**
	 * Converte uma lista de float em um array de floats.
	 * 
	 * @param list
	 * @return
	 */
	public static float[] listToArray(List<Float> list) {
		float flo[] = new float[list.size()];
		for (int i = 0; i < list.size(); i++) {
			flo[i] = list.get(i);
		}
		return flo;
	}

	/**
	 * Converte um Vec4 em um Vec3 ignorando o w
	 * 
	 * @param vec4
	 * @return
	 */
	public static Vector3f vec4ToVec3(Vector4f vec4) {
		return new Vector3f(vec4.x, vec4.y, vec4.z);
	}

}
