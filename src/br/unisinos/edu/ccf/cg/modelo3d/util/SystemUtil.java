package br.unisinos.edu.ccf.cg.modelo3d.util;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.joml.Vector3f;

/**
 * Classe utilitaria para funções de sistema.
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 30 de abr de 2020
 */
public class SystemUtil {

	/**
	 * Método que finaliza o programa por falhas/erros.
	 */
	public static void endProgram(String message) {
		JOptionPane.showMessageDialog(null, "Houve falha ao realizar a operação: \n" + message, "Atenção", JOptionPane.ERROR_MESSAGE);
		System.out.println("Finalizando aplicação....");
		System.exit(0);
	}

	/**
	 * Exibe uma mensagem em dialogo no sistema.
	 * 
	 * @param message
	 */
	public static void message(String message) {
		JOptionPane.showMessageDialog(null, "Aviso: \n" + message, "Atenção", JOptionPane.INFORMATION_MESSAGE);
	}

	public static boolean diffTime(long time1, long time2, long value) {
		return (time2 - time1) > value;
	}

	public static double calculoAngulo(Vector3f p1, Vector3f p2) {

		float W = p2.x - p1.x;
		float H = p2.y - p1.y;
		Double O = Math.atan(H / W);
		double angle;
		if (W < 0) {
			angle = O - Math.toRadians(90);
		} else {
			angle = O + Math.toRadians(90);
		}
		return angle;
	}

}
