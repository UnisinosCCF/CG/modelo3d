package br.unisinos.edu.ccf.cg.modelo3d.object;

/**
 * Classe que representa uma face do grupo.
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 1 de mai de 2020
 */
public class Face {

	// glm::vec3 (sao indices dos vertices na Malha (vertex))
	private int[] verts;
	// glm::vec3 (sao indices dos normais na Malha (normals))
	private int[] norms;
	// glm::vec2 (sao indices das texturas na Malha (mappings))
	private int[] texts;

	private int numberVertex;

	/**
	 * f 237/53/279 236/53/256 205/1/207 206/67/208 estes grupos simbolizam cada um
	 * um conjunto de indices, o primeiro é sempre vertice.
	 * 
	 * Tem que ser feito lógica para se carregar os formatos, são 3, ver no pDF da
	 * aula.
	 * 
	 */
	public Face(int numberVertex) {
		super();
		this.verts = new int[numberVertex];
		this.norms = new int[numberVertex];
		this.texts = new int[numberVertex];
		this.numberVertex = numberVertex;
	}

	public int[] getVerts() {
		return verts;
	}

	public void setVerts(int[] verts) {
		this.verts = verts;
	}

	public int[] getNorms() {
		return norms;
	}

	public void setNorms(int[] norms) {
		this.norms = norms;
	}

	public int[] getTexts() {
		return texts;
	}

	public void setTexts(int[] texts) {
		this.texts = texts;
	}

	public int getNumberVertex() {
		return numberVertex;
	}

}
