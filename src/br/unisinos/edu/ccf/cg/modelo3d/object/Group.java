package br.unisinos.edu.ccf.cg.modelo3d.object;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

import java.util.ArrayList;
import java.util.List;

import br.unisinos.edu.ccf.cg.modelo3d.model.TexturesManager;

/**
 * Classe que representa um grupo do obj.
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 1 de mai de 2020
 */
public class Group {

	private int vaoID;
	private int vboVertices;
	private int vboTextures;
	private int vboNormals;

	private List<Face> faces;
	private int numberVertices;

	/**
	 * Nome do Grupo no obj esta em: -> g nome do grupo
	 */
	private String name;

	/**
	 * id de material que sera usado
	 */
	private String material;

	/**
	 * 
	 */
	private Material materialObj;

	public Group(String name) {
		this.name = name;
		this.faces = new ArrayList<Face>();
	}

	private void bindTexture() {
		if (this.getMaterialObj() == null || this.getMaterialObj().getTID() == null) {

		} else {
			TexturesManager.getInstance().getTexture(this.getMaterialObj().getTID()).bind();
		}
	}

	/**
	 * Desenha o VAO do grupo.
	 */
	public void draw() {
		glBindVertexArray(vaoID);
		this.bindTexture();
		glDrawArrays(GL_TRIANGLES, 0, numberVertices);
		glBindVertexArray(0);
	}

	public Material getMaterialObj() {
		return materialObj;
	}

	public void setMaterialObj(Material materialObj) {
		this.materialObj = materialObj;
	}

	public int getVboVertices() {
		return vboVertices;
	}

	public void setVboVertices(int vboVertices) {
		this.vboVertices = vboVertices;
	}

	public int getVboTextures() {
		return vboTextures;
	}

	public void setVboTextures(int vboTextures) {
		this.vboTextures = vboTextures;
	}

	public int getVboNormals() {
		return vboNormals;
	}

	public void setVboNormals(int vboNormals) {
		this.vboNormals = vboNormals;
	}

	public void setVaoID(int vaoID) {
		this.vaoID = vaoID;
	}

	public int getNumberVertices() {
		return numberVertices;
	}

	public void setNumberVertices(int numberVertices) {
		this.numberVertices = numberVertices;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public int getVaoID() {
		return vaoID;
	}

	public List<Face> getFaces() {
		return faces;
	}

	public void addFace(Face face) {
		this.numberVertices += face.getNumberVertex();
		this.faces.add(face);
	}

	public void removeFace(Face face) {
		this.numberVertices -= face.getNumberVertex();
		this.faces.remove(face);
	}

}
