package br.unisinos.edu.ccf.cg.modelo3d.object;

import org.joml.Vector3f;

public class Material {

	private String nome;
	private Vector3f ka;
	private Vector3f kd;
	private Vector3f ks;
	private float Ns;
	private String map;
	private Integer TID;

	public Material(String nome) {
		super();
		this.nome = nome;
		this.ka = new Vector3f(0);
		this.kd = new Vector3f(0);
		this.ks = new Vector3f(0);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Vector3f getKa() {
		return ka;
	}

	public void setKa(Vector3f ka) {
		this.ka = ka;
	}

	public Vector3f getKd() {
		return kd;
	}

	public void setKd(Vector3f kd) {
		this.kd = kd;
	}

	public Vector3f getKs() {
		return ks;
	}

	public void setKs(Vector3f ks) {
		this.ks = ks;
	}

	public float getNs() {
		return Ns;
	}

	public void setNs(float ns) {
		Ns = ns;
	}

	public String getMap() {
		return map;
	}

	public void setMap(String mapKd) {
		this.map = mapKd;
	}

	public Integer getTID() {
		return TID;
	}

	public void setTID(Integer tID) {
		TID = tID;
	}

}
