package br.unisinos.edu.ccf.cg.modelo3d.object;

import java.util.List;

import org.joml.Vector3f;
import org.joml.Vector4f;

import br.unisinos.edu.ccf.cg.modelo3d.opengl.Transform;
import br.unisinos.edu.ccf.cg.modelo3d.util.FormatUtil;

public class Obj3D {

	// Malha
	private Mesh mesh;
	private String name;
	private Transform transform = new Transform();
	private Vector4f colors = new Vector4f(1);

	// Status de vida do objeto, n > 0 = numero de vidas restantes do obj; n = 0
	// objeto destruido, n < 0 objeto indestrutivel.
	private int variavel;

	// Diz se este objeto é desenhavel no main loop.
	private boolean drawThis = true;

	public boolean isDrawThis() {
		return drawThis;
	}

	public void setDrawThis(boolean drawThis) {
		this.drawThis = drawThis;
	}

	public String getName() {
		return name;
	}

	public Vector4f getColors() {
		return colors;
	}

	public void setColors(Vector4f colors) {
		this.colors = colors;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Mesh getMesh() {
		return mesh;
	}

	public void setMesh(Mesh mesh) {
		this.mesh = mesh;
	}

	public Transform getTransform() {
		return transform;
	}

	public void setTransform(Transform transform) {
		this.transform = transform;
	}

	public int getVariavel() {
		return variavel;
	}

	public void setVariavel(int variavel) {
		this.variavel = variavel;
	}

	public Vector3f getPM() {
		return (getMin().add(getMax(), new Vector3f()).div(2));
	}

	public float getR() {
		return (getMin().distance(getMax())) / 2;
	}

	public Vector3f getC() {
		return getPM();
	}

	// Para Colisao
	public Vector3f getMin() {
		return FormatUtil.vec4ToVec3(new Vector4f(this.mesh.getMinPointer(), 1).mul(this.getTransform().getTransformation()));
	}

	public Vector3f getMax() {
		return FormatUtil.vec4ToVec3(new Vector4f(this.mesh.getMaxPointer(), 1).mul(this.getTransform().getTransformation()));
	}

}
