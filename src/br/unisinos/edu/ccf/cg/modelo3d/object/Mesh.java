package br.unisinos.edu.ccf.cg.modelo3d.object;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;

import br.unisinos.edu.ccf.cg.modelo3d.util.SystemUtil;

public class Mesh {

	private Vector3f minPointer;
	private Vector3f maxPointer;

	private String name;

	// Texturas, Vertices, normais e grupos.
	private List<Vector2f> mappings;
	private List<Vector3f> vertex;
	private List<Vector3f> normals;
	private List<Group> groups;

	public Vector3f getMinPointer() {
		return minPointer;
	}

	public void setMinPointer(Vector3f minPointer) {
		this.minPointer = minPointer;
	}

	public Vector3f getMaxPointer() {
		return maxPointer;
	}

	public void setMaxPointer(Vector3f maxPointer) {
		this.maxPointer = maxPointer;
	}

	public Mesh(String name) {
		this.name = name;
		this.mappings = new ArrayList<Vector2f>();
		this.vertex = new ArrayList<Vector3f>();
		this.normals = new ArrayList<Vector3f>();
		this.groups = new ArrayList<Group>();
		this.minPointer = new Vector3f();
		this.maxPointer = new Vector3f();
	}

	public void drawGroups() {
		for (Group group : this.groups) {
			group.draw();
		}
	}

	/**
	 * Busca um grupo caso exista, ou então o cria como novo.
	 * 
	 * @param id
	 * @return
	 */
	public Group getGroupUnity(String id) {
		for (int i = 0; i < groups.size(); i++) {
			if (groups.get(i).getName().equals(id)) {
				return groups.get(i);
			}
		}
		// Cria novo Grupo
		Group group = new Group(id);
		this.groups.add(group);
		return group;
	}

	public String getName() {
		return name;
	}

	public List<Vector2f> getMappings() {
		return mappings;
	}

	public List<Vector3f> getVertex() {
		return vertex;
	}

	public List<Vector3f> getNormals() {
		return normals;
	}

	public List<Group> getGroups() {
		return groups;
	}

	/**
	 * Método responsavel por adicionar um novo vertice a malha.
	 * 
	 * @param coords
	 * @return
	 */
	public Mesh addVertex(float[] coords) {
		if (coords.length != 3) {
			SystemUtil.endProgram("Numero de pontos xyz do vertice não são validos.");
		}
		return addVertex(coords[0], coords[1], coords[2]);
	}

	/**
	 * Metodo responsavel por adicionar um novo vertice a malha. já verifica o
	 * maxPointer e o minPointer para essa malha.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public Mesh addVertex(float x, float y, float z) {
		Vector3f vector3f = new Vector3f(x, y, z);
		verifyMaxMinXYZ(vector3f);
		this.vertex.add(vector3f);
		return this;
	}

	/**
	 * Adiciona uma nova coordenada de textura na malha.
	 * 
	 * @param coords
	 * @return
	 */
	public Mesh addMapping(float[] coords) {
		if (coords.length != 2) {
			SystemUtil.endProgram("Numero de pontos xy do mapping de textura não são validos.");
		}
		return addMapping(coords[0], coords[1]);
	}

	/**
	 * Adiciona uma nova coordenada de textura na malha.
	 * 
	 * @param coords
	 * @return
	 */
	public Mesh addMapping(float x, float y) {
		this.mappings.add(new Vector2f(x, y));
		return this;
	}

	/**
	 * Adiciona uma nova coordenada de normal na malha.
	 * 
	 * @param coords
	 * @return
	 */
	public Mesh addNormal(float x, float y, float z) {
		this.normals.add(new Vector3f(x, y, z));
		return this;
	}

	/**
	 * Adiciona uma nova coordenada de normal na malha.
	 * 
	 * @param coords
	 * @return
	 */
	public Mesh addNormal(float[] coords) {
		if (coords.length != 3) {
			SystemUtil.endProgram("Numero de pontos xyz do vertice de normais não são validos.");
		}
		return addNormal(coords[0], coords[1], coords[2]);
	}

	/**
	 * Metodo para verificar se o X é o novo max ou min.
	 * 
	 * @param x
	 */
	public void verifyMaxMinX(float x) {
		if (x > this.maxPointer.x) {
			this.maxPointer.x = x;
		} else if (x < this.minPointer.x) {
			this.minPointer.x = x;
		}
	}

	/**
	 * Metodo para verificar se o X é o novo max ou min.
	 * 
	 * @param y
	 */
	public void verifyMaxMinY(float y) {
		if (y > this.maxPointer.y) {
			this.maxPointer.y = y;
		} else if (y < this.minPointer.y) {
			this.minPointer.y = y;
		}
	}

	/**
	 * Metodo para verificar se o Z é o novo max ou min.
	 * 
	 * @param z
	 */
	public void verifyMaxMinZ(float z) {
		if (z > this.maxPointer.z) {
			this.maxPointer.z = z;
		} else if (z < this.minPointer.z) {
			this.minPointer.z = z;
		}
	}

	/**
	 * Metodo para verificar se o vertice atual possuem algum ponto para ser usado
	 * no maxPointer ou minPointer.
	 * 
	 * @param vector3f
	 */
	public void verifyMaxMinXYZ(Vector3f vector3f) {
		verifyMaxMinX(vector3f.x);
		verifyMaxMinY(vector3f.y);
		verifyMaxMinZ(vector3f.z);
	}
}
