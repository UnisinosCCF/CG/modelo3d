package br.unisinos.edu.ccf.cg.modelo3d.controller;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_R;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_X;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_Z;
import static org.lwjgl.glfw.GLFW.glfwGetKey;
import static org.lwjgl.glfw.GLFW.glfwGetTime;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glEnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Vector3f;

import br.unisinos.edu.ccf.cg.modelo3d.model.ReadValues;
import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig;
import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig.MeshConfig;
import br.unisinos.edu.ccf.cg.modelo3d.model.SceneConfig.Object3DConfig;
import br.unisinos.edu.ccf.cg.modelo3d.model.ShotModel;
import br.unisinos.edu.ccf.cg.modelo3d.model.TexturesManager;
import br.unisinos.edu.ccf.cg.modelo3d.object.Group;
import br.unisinos.edu.ccf.cg.modelo3d.object.Material;
import br.unisinos.edu.ccf.cg.modelo3d.object.Mesh;
import br.unisinos.edu.ccf.cg.modelo3d.object.Obj3D;
import br.unisinos.edu.ccf.cg.modelo3d.opengl.Camera;
import br.unisinos.edu.ccf.cg.modelo3d.opengl.Shader;
import br.unisinos.edu.ccf.cg.modelo3d.opengl.VertexBuild;
import br.unisinos.edu.ccf.cg.modelo3d.opengl.Window;

/**
 * Classe principal de execução do projeto.
 * 
 * @author Cristiano Farias <cristianocostafarias@gmail.com>
 * @since 8 de abr de 2020
 */
public class Main {

	private SceneConfig config;
	private String shaderPath;
	private Window window;
	private Shader shader;
	private Camera camera;
	private List<Mesh> meshes;
	private List<Obj3D> objects;
	private ShotModel shot;

	float deltaTime = 0.0f;
	float lastFrame = 0.0f;

	// Lista de materiais
	private Map<String, String> materialNames;
	private List<Material> materialLib;

	public Main(String shaderName) {
		this.shaderPath = shaderName;
		this.meshes = new ArrayList<Mesh>();
		this.objects = new ArrayList<Obj3D>();
		this.materialNames = new HashMap<String, String>();
		this.materialLib = new ArrayList<Material>();
	}

	/**
	 * Método de start do projeto. cria a janela, inicia a opengl e cria a camera.
	 * 
	 * @param configPath
	 * @param windowName
	 */
	public void start(String configPath, String windowName) {

		// faz a leitura do arquivo de configuração.
		config = ReadValues.readConfig(configPath);
		// Inicia a Janela
		this.window = new Window(windowName, config.getWindow());
		window.createWindow();
		// Inicia o Shader
		this.shader = new Shader(this.shaderPath);
		shader.create();
		// Carregar meshes
		for (MeshConfig mc : config.getMeshes()) {
			Mesh mesh = ReadValues.readMesh(mc.getName(), mc.getPath(), materialNames);

			// Constrói os VBOs e o VAO de cada grupo.
			VertexBuild.buildVectors(mesh);
			this.meshes.add(mesh);
		}

		// Carrega Materials
		for (String materialString : materialNames.values()) {
			this.materialLib.addAll(ReadValues.readMaterial(materialString));
		}

		// bUILD MATERIALS
		this.buildMTL(this.meshes, this.materialLib);

		// Carregar objetos
		for (Object3DConfig obj3D : config.getObjs()) {
			Obj3D ob = ReadValues.mountObject3D(obj3D.getName(), obj3D.getMeshName(), this.meshes);
			ob.getTransform().setPosition(new Vector3f(obj3D.getTranslationStart()));
			ob.getTransform().getRotation().rotateY((float) Math.toRadians(obj3D.getRotationStart()));
			ob.getTransform().setScale(new Vector3f(obj3D.getScaleStart()));
			ob.setVariavel(obj3D.getVariable());

			// verifica se tem arquivo de animação
			if (obj3D.getVariable2() != null) {
				// Carregando arquivo animação
				ob.getTransform().setAnimation(ReadValues.readAnimacao(obj3D.getVariable2()), obj3D.getScaleStart());
				ob.getColors().add(0f, 0.7f, 0f, 0);
			}

			this.objects.add(ob);
		}

		// Carrega a classe de controle do tiro.
		this.shot = new ShotModel().start(meshes);
		this.objects.add(shot.getObject());

		// Inicia a camera
		camera = new Camera(config.getCamera().getEye(), config.getCamera().getDir(), config.getCamera().getUp());

		// Chama o draw
		drawScene();

	}

	/**
	 * Constrói e associa o objeto MTl referente a cada GRUPO
	 */
	public void buildMTL(List<Mesh> meshs, List<Material> materials) {
		for (int i = 0; i < materials.size(); i++) {
			if (materials.get(i).getMap() != null) {
				Integer id = TexturesManager.getInstance().newTexture(materials.get(i).getMap());
				materials.get(i).setTID(id);
			}
			for (Mesh ms : meshs) {
				for (Group group : ms.getGroups()) {
					if (group.getMaterial() != null && group.getMaterial().equals(materials.get(i).getNome())) {
						group.setMaterialObj(materials.get(i));
					}
				}
			}
		}
	}

	/**
	 * Método que verifica movimentação na cena.
	 */
	private void checkMotion() {
		if (glfwGetKey(window.getID(), GLFW_KEY_UP) == GL_TRUE) {
			camera.processKeyboard(0, deltaTime);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_DOWN) == GL_TRUE) {
			camera.processKeyboard(1, deltaTime);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_A) == GL_TRUE) {
			camera.processKeyboard(2, deltaTime);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_D) == GL_TRUE) {
			camera.processKeyboard(3, deltaTime);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_LEFT) == GL_TRUE) {
			camera.processKeyboard(4, deltaTime);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_RIGHT) == GL_TRUE) {
			camera.processKeyboard(5, deltaTime);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_R) == GL_TRUE) {
			shot.rotationX(10);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_Z) == GL_TRUE) {
			shot.sumSpeed(-0.0001f);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_X) == GL_TRUE) {
			shot.sumSpeed(+0.0001f);

		} else if (glfwGetKey(window.getID(), GLFW_KEY_SPACE) == GL_TRUE) {
			shot.fire(camera.getPosition().get(new Vector3f()), new Vector3f(camera.getFront()), camera.getYaw() + 90);

		}

		// Proximo state da bala
		shot.next(objects);

	}

	/**
	 * Metodo de desenho da cena.
	 */
	private void drawScene() {

		glEnable(GL_DEPTH_TEST);
		boolean active = true;

		while (active) {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			active = window.update();

			float currentFrame = (float) glfwGetTime();
			deltaTime = currentFrame - lastFrame;
			lastFrame = currentFrame;

			// Metodo que verifica
			checkMotion();
			for (int i = 0; i < objects.size(); i++) {
				if (objects.get(i).isDrawThis()) {

					if (objects.get(i).getTransform().getAnimation() != null && !objects.get(i).getTransform().getAnimation().isEmpty()) {
						objects.get(i).getTransform().nextAnimation();
					}

					Mesh mesh = objects.get(i).getMesh();
					shader.useShader();
					shader.setCamera(camera, window);
					shader.setModel(objects.get(i).getTransform());
					shader.setColor(objects.get(i).getColors());
					// objects.get(i).texBind(0);
					shader.setUniform("sampler", 0);

					mesh.drawGroups();

				}
			}
			window.swapBuffers();
			// FPS
			window.updateFPS();

		}

		// Ver depois
		// test.destroy();
		shader.destroy();

		window.free();
	}

	/**
	 * Start classe application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		new Main("shader").start("config/sceneConfig.cfg", "Meu Modelo 3D - Cristiano Farias");

		System.out.println("Finalizado........");
	}

}
