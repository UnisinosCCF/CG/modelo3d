#version 410

uniform sampler2D sampler;
out vec4 fragColor;
in vec2  tex_coords;
in vec4 f_colors;

void main(){
	fragColor = texture(sampler, tex_coords) * f_colors;
}

