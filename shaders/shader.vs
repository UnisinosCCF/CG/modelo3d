#version 410

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textures;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec4 colors;

out vec2 tex_coords;
out vec4 f_colors;

void main(){	
	f_colors = colors;
	tex_coords = textures;
	gl_Position = projection * view *  model  * vec4(position,1.0f);
}

